<?php
require '.\AWS\autoload.php';
$target_file = './uploads/' . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$title = $_POST['title'];
$description = $_POST['description'];
$maxLengthTitle = 100;

if (strlen($title) > $maxLengthTitle) {
    $stringCut = substr($title, 0, $maxLengthTitle);
    $title = substr($stringCut, 0, strrpos($stringCut, ' ')); 
}
$maxLengthDescription = 500;

if (strlen($description) > $maxLengthDescription) {
    $stringCut = substr($description, 0, $maxLengthDescription);
    $description = substr($stringCut, 0, strrpos($stringCut, ' ')); 
}
$videoFileType = pathinfo($target_file,PATHINFO_EXTENSION);
$uploaded = './uploads/' . date("Y-m-d-h-i-s"). '.' .$videoFileType;
$uploadedhtml = './uploads/' . base_convert(time(), 10, 36) . '.php';
	if($videoFileType != "mp4") {
    die("Sorry, only .MP4 files are allowed. <a href=\"/\">Go Back</a>");
    $uploadOk = 0;
}
   if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
	   $myfile = fopen($uploadedhtml , "w") or die("Unable to create page for your file!");
$content = "<!doctype html><html class=\"no-js\" lang=\"en-US\"><head> <meta charset=\"utf-8\"> <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\"> <title>Uploader</title> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"><meta name=\"apple-mobile-web-app-capable\" content=\"yes\"><meta name=\"apple-mobile-web-app-status-bar-style\" content=\"light\"><meta name=\"apple-mobile-web-app-title\" content=\"PHP Uploader\"><link rel=\"apple-touch-icon\" href=\"/apple-touch-icon.png\"> <link rel=\"stylesheet\" href=\"//cdn.elpoposes.com/uploader_css_v1/bootstrap.min.css\"> <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300' rel='stylesheet' type='text/css'> <style>body{padding-top: 50px; padding-bottom: 20px;}</style> <link rel=\"stylesheet\" href=\"//cdn.elpoposes.com/uploader_css_v1/bootstrap-theme.min.css\"> <link rel=\"stylesheet\" href=\"//cdn.elpoposes.com/uploader_css_v1/main.css\"> <script src=\"//cdn.elpoposes.com/upload_js_v1/modernizr-2.8.3-respond-1.4.2.min.js\"></script> <link href=\"//vjs.zencdn.net/5.8/video-js.min.css\" rel=\"stylesheet\"><script src=\"https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js\"></script></head><body> <div class=\"container\"> <h1 class=\"text-primary text-center\"><a href='/Uploader/' style='text-decoration:none;color:#000;'>Uploader</a></h1> <div class=\"panel panel-default\"> <div class=\"panel-body\"> <div class=\"container-fluid\"><p class=\"title\">$title</p><div class=\"embed-responsive embed-responsive-16by9\"><video class=\"video-js embed-responsive-item\" autoplay loop controls data-setup=\"{}\"> <source src=\"//cdn.elpoposes.com/$uploaded\" type=\"video/mp4\"> <p class=\"vjs-no-js\"> To view this video please enable JavaScript, and consider upgrading to a web browser that <a href=\"http://videojs.com/html5-video-support/\" target=\"_blank\">supports HTML5 video</a> </p></video></div><div class=\"well text-left\"><p class=\"text-info\">Uploaded on: ". date ('M d, Y')."</p><p class=\"description\">$description</p></div></div></div></div> <hr> <footer></footer> </div><script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js\"></script>  <script src=\"https://vjs.zencdn.net/5.8.7/video.js\"></script> </script> <script src=\"//cdn.elpoposes.com/upload_js_v1/bootstrap.min.js\"></script></body></html>";
fwrite($myfile, $content);  
fclose($myfile); 
header('Location: '.$uploadedhtml);
} 
else
{ 
	echo "There was an error uploading your video, plesse try again. <a href=\"/\">Go Back</a>";
	echo "<br>";
	foreach (getallheaders() as $name => $value) {
    echo "$name: $value\n";
	}
}
use .\AWS\S3\S3Client;

$bucket = 'poposes';
	
// Instantiate the Amazon Web Services Client
$config = array(
    'region' => 'us-east-1',
		'version' => 'latest',
		'credentials' => array(
 'key' => '',
    'secret' => '/LQDEg',
  )
);
$s3 = S3Client::factory($config);


// Upload a file.
   $s3->putObject(array(
    'Bucket'       => $bucket,
    'SourceFile'   => $target_file,
	'Key' 		   => $uploaded, 
    'ACL'          => 'public-read',
    'StorageClass' => 'STANDARD'
));
unlink($target_file);
?>
