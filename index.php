
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en-US"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en-US"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!-->
 <html class="no-js" lang="en-US"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Uploader</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
		<link rel="stylesheet" href="//cdn.elpoposes.com/uploader_css_v1/bootstrap.min.css">
        <link rel="stylesheet" href="//cdn.elpoposes.com/uploader_css_v1/animate.min.css">
        <link rel="stylesheet" href="//cdn.elpoposes.com/uploader_css_v1/bootstrap-theme.min.css">
		<link rel="stylesheet" href="//cdn.elpoposes.com/uploader_css_v1/main.css">
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
		<script src="//cdn.elpoposes.com/upload_js_v1/jquery-1.11.2.min.js"></script>
        <script src="//cdn.elpoposes.com/upload_js_v1/modernizr-2.8.3-respond-1.4.2.min.js"></script>
		<script src="//cdn.elpoposes.com/upload_js_v1/bootstrap.min.js"></script>
		</head> 
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div class="container">

  	<h1 class="text-primary text-center animated fadeInUp">Uploader</h1>
	<hr>
    <div class="text-center">	        
	</div>
	<div class="container-fluid">
	<form role="form" action="upload-vid.php" method="post" enctype="multipart/form-data">
	<input type="text" name="title" id="title" placeholder="Title (Optional) 100 characters max." maxlength="100" class="form-control" autocomplete="off">
	<div id="title_count"></div>
	<br>
		<input type="text" name="description" id="description" maxlength="500" placeholder="Description (Optional) 500 characters max." class="form-control" autocomplete="off">
		<div id="description_count"></div>
		  <div class="form-group">
		  <br>
		  	    <p>Select video to upload: (only .MP4, max 100 MB)</p>
	<input id="upload-file" type="file" name="fileToUpload" accept="video/*" id="fileToUpload">
	<button type="button" class="btn btn-default" id="fileupload">Choose your file</button>
	</div>
    <input class="btn btn-md" type="submit" value="Upload" name="submit"> 
	</form>
 </div>
      <hr>
      <footer> 
        <p>Max upload size: 100 MB</p> 
      </footer>
    </div>
	<!-- /container -->      
        <script src="//cdn.elpoposes.com/upload_js_v1/main.js"></script>
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MM4RVS"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-MM4RVS');</script>
    </body>
</html>