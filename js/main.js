$(document).ready(function (){
    validate();
	    $('#url').change(validate);
});

function validate(){
    if (
        $('#url').val().length    >   0) {
        $("#suburl").prop("disabled", false);
		$("#jsub").remove();
    }
    else {
        $("#suburl").prop("disabled", true);
		$("#jsub").text("You must submit a URL and press Enter or click Submit");
    }
}